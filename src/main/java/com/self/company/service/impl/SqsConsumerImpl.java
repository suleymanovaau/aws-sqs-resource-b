package com.self.company.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.aws.messaging.config.annotation.EnableSqs;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SqsConsumerImpl {

    private Logger logger =  LoggerFactory.getLogger(this.getClass());

    static final String QUEUE_NAME = "my-aws-queue.fifo";


    @SqsListener("my-aws-queue.fifo")
    public void listenToQueue(String message){
        logger.info("Received message = " + message);
    }


}
