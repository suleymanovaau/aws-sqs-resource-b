package com.self.company.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "cloud.aws")
@Data
public class AwsProperties {
    private String region;
    private String queue;
    private Credentials credentials;
}
