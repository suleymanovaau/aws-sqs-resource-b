package com.self.company;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwsSqsResourceBApplication {

    public static void main(String[] args) {
        SpringApplication.run(AwsSqsResourceBApplication.class, args);
    }

}
